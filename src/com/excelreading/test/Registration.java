package com.excelreading.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Registration {

	public static void main(String[] args) throws InterruptedException, IOException, BiffException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		WebElement languagefield;
		WebElement language;
		WebElement skills;
		WebElement selectyearbox;
		WebElement selectcountry;
		WebElement selectmonthbox;
		WebElement selectdaybox;

		driver.manage().window().maximize();

		driver.get("http://demo.automationtesting.in/Register.html");

		Thread.sleep(2000);
		
		//Read the excel file

		FileInputStream fs = new FileInputStream("./testdata/Testdataregistration.xls");
		
		Workbook wb = Workbook.getWorkbook(fs);
		
		Sheet sh = wb.getSheet("Sheet1");

		int totalNorows = sh.getRows();

		int totalNocols = sh.getColumns();

		for (int row = 1; row < totalNorows; row++) {

			String firstname = sh.getCell(0, row).getContents();

			String lastname = sh.getCell(1, row).getContents();

			String email = sh.getCell(2, row).getContents();

			String phoneNo = sh.getCell(3, row).getContents();

			String password = sh.getCell(4, row).getContents();

			String confirmpassword = sh.getCell(5, row).getContents();

			WebElement objfirstname = driver.findElement(By.xpath("//input[@placeholder='First Name']"));

			objfirstname.sendKeys(firstname);

			WebElement objlastname = driver.findElement(By.xpath("//input[@placeholder='Last Name']"));

			objlastname.sendKeys(lastname);

			WebElement objemail = driver.findElement(By.xpath("//input[@type='email']"));

			objemail.sendKeys(email);

			WebElement objphone = driver.findElement(By.xpath("//input[@type='tel']"));

			objphone.sendKeys(phoneNo);

			Thread.sleep(2000);

			WebElement objpassword = driver.findElement(By.xpath("//input[@id='firstpassword']"));

			objpassword.sendKeys(password);

			WebElement objconfirmpassword = driver.findElement(By.xpath("//input[@id='secondpassword']"));

			objconfirmpassword.sendKeys(confirmpassword);

			if (row == 1) {

				List<WebElement> oRadioButton = driver.findElements(By.name("radiooptions"));

				System.out.println(oRadioButton.size());

				boolean bValue = true;

				// This statement will return True, in case of first Radio button is selected

				bValue = oRadioButton.get(0).isSelected();

				if (bValue = false) {

					oRadioButton.get(0).click();

					System.out.println("Male is selected");

				} else {
					oRadioButton.get(1).click();
					System.out.println("Female is selected");
				}

				languagefield = driver.findElement(By.xpath("//div[@id='msdd']"));

				languagefield.click();

				language = driver.findElement(By.xpath("//a[contains(text(),'Czech')]"));

				language.click();

				skills = driver.findElement(By.xpath("//select[@id='Skills']"));
				Select skillsdropdown = new Select(skills);
				skillsdropdown.selectByVisibleText("APIs");

				Thread.sleep(4000);

				selectcountry = driver.findElement(By.xpath("//select[@id='countries']"));
				Select countryselection = new Select(selectcountry);
				countryselection.selectByValue("Albania");

				selectyearbox = driver.findElement(By.xpath("//select[@id='yearbox']"));
				Select yearselection = new Select(selectyearbox);
				yearselection.selectByValue("1920");

				selectmonthbox = driver.findElement(By.xpath("//select[@placeholder='Month']"));
				Select monthselection = new Select(selectmonthbox);
				monthselection.selectByIndex(10);

				selectdaybox = driver.findElement(By.xpath("//select[@id='daybox']"));
				Select dayselection = new Select(selectdaybox);
				dayselection.selectByIndex(12);

				Thread.sleep(4000);

			}

			else {

				List<WebElement> oRadioButton = driver.findElements(By.name("radiooptions"));

				System.out.println(oRadioButton.size());

				boolean bValue = true;

				// This statement will return True, in case of first Radio button is selected

				bValue = oRadioButton.get(0).isSelected();

				if (bValue = true) {

					oRadioButton.get(0).click();

					System.out.println("Male is selected");

				} else {
					oRadioButton.get(1).click();
					System.out.println("Female is selected");
				}

				languagefield = driver.findElement(By.xpath("//div[@id='msdd']"));

				languagefield.click();

				language = driver.findElement(By.xpath("//a[contains(text(),'Danish')]"));

				language.click();

				skills = driver.findElement(By.xpath("//select[@id='Skills']"));
				Select skillsdropdown = new Select(skills);
				skillsdropdown.selectByVisibleText("Adobe InDesign");

				Thread.sleep(4000);

				selectcountry = driver.findElement(By.xpath("//select[@id='countries']"));
				Select countryselection = new Select(selectcountry);
				countryselection.selectByValue("Argentina");

				selectyearbox = driver.findElement(By.xpath("//select[@id='yearbox']"));
				Select yearselection = new Select(selectyearbox);
				yearselection.selectByValue("1960");

				selectmonthbox = driver.findElement(By.xpath("//select[@placeholder='Month']"));
				Select monthselection = new Select(selectmonthbox);
				monthselection.selectByIndex(11);

				selectdaybox = driver.findElement(By.xpath("//select[@id='daybox']"));
				Select dayselection = new Select(selectdaybox);
				dayselection.selectByIndex(25);

				Thread.sleep(4000);

			}

			WebElement submitbutton = driver.findElement(By.xpath("//button[@id='submitbtn']"));
			submitbutton.click();

			Thread.sleep(4000);

			WebElement registertab = driver.findElement(By.xpath("//a[contains(text(),'Register')]"));

			registertab.click();
       
		}
		
		driver.close();
	}
}
